from fenics import *

def get_realspace_var(u, num) :
    V = u.function_space()
    comm = V.mesh().mpi_comm()
    dofmap = V.sub(num).dofmap()
    val_dof = dofmap.cell_dofs(0)[0]
    # the owner of the dof broadcasts the value
    own_range = dofmap.ownership_range()
    # broadcast not available in python
    if val_dof < own_range[1] and val_dof > own_range[0] :
        val_local = u.vector()[val_dof]
    else :
        # receive
        val_local = 0.0
    
    val = MPI.sum(comm, val_local)

    return val

def set_realspace_var(u, num, val) :
    V = u.function_space()
    comm = V.mesh().mpi_comm()
    dofmap = V.sub(num).dofmap()
    val_dof = dofmap.cell_dofs(0)[0]
    # the owner of the dof broadcasts the value
    own_range = dofmap.ownership_range()
    # broadcast not available in python
    if val_dof < own_range[1] and val_dof > own_range[0] :
        u.vector()[val_dof] = val

