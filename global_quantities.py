from dolfin import *
from lv_problem import *
from prolate_geometry import *
from os.path import expanduser
from textwrap import dedent
import cPickle
import numpy as np

parameters.allow_extrapolation = True

def load_displacements(case, stages,
                       datadir = "../data",
                       meshdir = "./meshes",
                       resdir  = "~/Dropbox/lvchallenge/_fenics/results_revision"):

    params = LVProblem.default_parameters()
    params["geometry"]["case"] = case
    params["geometry"]["meshdir"] = expanduser(meshdir)
    problem = LVProblem(**params)

    problem.set_control_mode("pressure")
    h5_load = HDF5File(mpi_comm_world(), 
            "{}/{}_full_cycle_u_save.h5".format(expanduser(resdir), case), "r")
    h5_load.read(problem.state, "/initial")
    
    markers = cPickle.load(open("{}/{}_full_cycle_pv.cpickle"\
            .format(expanduser(resdir), case)))["stage_markers"]

    ret = []
    for stage in stages:
        if markers[stage] != 0 :
            h5_load.read(problem.state.vector(), "values_{}".format(markers[stage]), True)
        ret.append(problem.state.sub(0, deepcopy=True))
        
    return problem.geo, ret

def compute_global_quantities(geo_sim, geo_tgt, u) :

    res = { 'sim_inner_volume' : geo_sim.inner_volume(u),
            'tgt_inner_volume' : geo_tgt.inner_volume(),
            'sim_wall_volume' : geo_sim.wall_volume(u),
            'tgt_wall_volume' : geo_tgt.wall_volume(),
            'sim_length_endo' : geo_sim.ventricle_axial_length('endo', u),
            'tgt_length_endo' : geo_tgt.ventricle_axial_length('endo'),
            'sim_length_epi' : geo_sim.ventricle_axial_length('epi', u),
            'tgt_length_epi' : geo_tgt.ventricle_axial_length('epi'),
            'sim_radius_endo' : geo_sim.average_radius('endo', 1.0, u),
            'tgt_radius_endo' : geo_tgt.average_radius('endo', 1.0),
            'sim_radius_epi' : geo_sim.average_radius('epi', 1.0, u),
            'tgt_radius_epi' : geo_tgt.average_radius('epi', 1.0),
            'distance_endo' : surf_distance(geo_sim, geo_tgt, 'endo', u),
            'distance_epi'  : surf_distance(geo_sim, geo_tgt, 'epi', u) }
    
    # add differences
    for qt in [ 'inner_volume', 'wall_volume', 'length_endo', 'length_epi',
                'radius_endo', 'radius_epi' ] :
        tgt = res['tgt_{}'.format(qt)] 
        sim = res['sim_{}'.format(qt)] 
        res['diff_{}'.format(qt)] = 100.0 * (tgt - sim)/tgt

    return res

if __name__ == "__main__" :

    stages = [ 'ED', 'ES' ]
    cases  = [ '0912', '0917', '1017', '1024' ]

    for case in cases :
        geo_sim, ulist = load_displacements(case, stages)
        for stage, u in zip(stages, ulist) :
            print 'CASE {}, STAGE {}'.format(case, stage)
            geo_tgt_p = geo_sim.parameters
            geo_tgt_p['stage'] = stage
            geo_tgt = ProlateGeometry(**geo_tgt_p)
            qt = compute_global_quantities(geo_sim, geo_tgt, u)
            print dedent(\
            """\
                                (ref)    (sim)
            inner volume    {tgt_inner_volume:8.3f} {sim_inner_volume:8.3f}
            wall volume     {tgt_wall_volume:8.3f} {sim_wall_volume:8.3f}
            length (endo)   {tgt_length_endo:8.3f} {sim_length_endo:8.3f}
            length (epi)    {tgt_length_epi:8.3f} {sim_length_epi:8.3f}
            radius (endo)   {tgt_radius_endo:8.3f} {sim_radius_endo:8.3f}
            radius (epi)    {tgt_radius_epi:8.3f} {sim_radius_epi:8.3f}

            distance (endo) {distance_endo:8.3f}
            distance (epi)  {distance_epi:8.3f}
            """).format(**qt)

