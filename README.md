Source for the 'STACOM 2014 LV Challenge', based on FEniCS.

Requirements
------------
* FEniCS 1.4.0+
* VTK
* Mayavi

Remarks
-------
* Preprocessed geometry should be downloaded first using
  ```meshes/download_meshes.sh``` from inside the ```meshes``` directory.
* The main code for the mechanical solver starts in ```itertarget.py```.
* Post-processing is in ```global_quantities.py```.

Authors
-------
* Johan Hake
* Simone Pezzuto
