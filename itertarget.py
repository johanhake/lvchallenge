import scipy.interpolate as interp
import operator as op
import matplotlib.pyplot as plt

from lv_problem import LVProblem
from lv_solver import LVSolver
from data_collector import DataCollector
from data_io import *
from fenics import *

def load_problem_from_passive(case, suffix = "",
                                    meshdir = "./meshes",
                                    datadir = "./data",
                                    resdir  = "./results"):

    print "LOADING SAVED VALUES FOR", case
    import cPickle
    outbase_passive = "{}/{}_passive{}".format(resdir, case, suffix)

    params = LVChallengeProblem.default_parameters()
    params["geometry"]["case"] = case
    params["geometry"]["meshdir"] = meshdir
    params["geometry"]["datadir"] = datadir
    params["control_volume"] = False

    problem = LVChallengeProblem(**params)
    problem.set_control_mode("pressure")
    
    # load the last step of passive inflation
    h5_load = HDF5File(mpi_comm_world(), 
            "{}_u_save.h5".format(outbase_passive), "r")
    h5_load.read(problem.state, "/initial")
    p_atrium = read_target_pressure(case, datadir, stage = "ED")
    p_atrium /= 10.0
    problem.set_pendo(p_atrium)
    control_parameter = None
    with open("{}_pv.cpickle".format(outbase_passive), "r") as f :
        data = cPickle.load(f)
        last_step = len(data["Vlist"]) - 1
        data.pop("Vlist")
        data.pop("plist")
        if data:
            control_parameter = data.keys()[0].replace("_list", "")
            control_value = data.values()[0][-1]
            print "Setting control parameter: {} = {}".format(\
                control_parameter, control_value)
            problem.set_control_parameters(**{control_parameter:control_value})
        
    h5_load.read(problem.state.vector(), "/values_{}".format(last_step), True)

    return problem

           
def iter_target(problem, collector, target_end, target_parameter="pressure",
                control_step=0.2, control_parameter="pressure",
                control_mode="pressure"):

    solver = LVSolver()

    problem.set_control_mode(control_mode)

    # Sanity checks
    if control_parameter in ["pressure", "volume"] and control_parameter != control_mode:
        error("control mode and control parameter have to be the same.")

    if target_parameter != control_parameter:
        if target_parameter not in ["volume", "pressure"]:
            error("expected target_parameter to be one of volume or pressure "\
                  "if different from control parameter")
        if target_parameter == control_mode:
            error("target parameter cannot be the same as the control mode")
    
    target_value = problem.get_control_parameter(target_parameter)
    control_value = problem.get_control_parameter(control_parameter)

    comp = op.gt if target_value < target_end else op.lt

    print 
    print "ITER TARGET", problem.parameters["geometry"]["case"], target_parameter, target_end

    collector.output_params()

    # Some flags
    target_reached = False
    save_me = True
    iterating = False
    first_time = True
    interp_target_values = []
    interp_control_values = []
    while not target_reached:
        old_target_value = target_value
        old_control_value = control_value
        
        state_old = problem.state.copy(True)

        # If same target and control param and we are getting close
        if target_parameter == control_parameter and \
               comp(control_value + control_step, target_end):
            control_value = target_end
            
        else:
            
            # If control step brings us below 0 for control parameter
            if control_value + control_step < 0.0 or abs(control_value + control_step)<1e-12:
                control_step = -control_value/2
            
            control_value += control_step
        
        if abs((target_value - target_end)/target_end) <  0.001:
            print 
            print "TARGET REACHED {}={}".format(target_parameter, target_value)
            target_reached = True
            save_me = True

        # Set control parameter before an iteration
        if not target_reached:
            print 
            print "TRYING NEW CONTROL VALUE: {}={}".format(control_parameter, \
                                                           control_value)
            problem.set_control_parameters(**{control_parameter:control_value})
            try:
                nliter, nlconv = solver.solve(problem)
            except RuntimeError:
                print 
                print "NOT CONVERGING" 
                print "REDUCING step"
            
                # Reset solution
                problem.state.assign(state_old)
                control_value = old_control_value
            
                # Reduce step
                control_step *= 0.5
                continue

            target_value = problem.get_control_parameter(target_parameter)

            # If we are going in the wrong direction
            if first_time and not iterating and \
                   abs(target_value - target_end) > abs(old_target_value - target_end):

                print "STEPING IN WRONG DIRECTION"
                # Reset solution
                problem.state.assign(state_old)
                control_value = old_control_value
            
                # Reduce step
                control_step *= -1
                continue
                
        first_time = False

        # Output succesfull step
        collector.output_params()

        # Check if target has been reached
        if abs((target_value - target_end)/target_end) <  0.001:
            print 
            print "TARGET REACHED {}={}".format(target_parameter, target_value)
            target_reached = True
            save_me = True

        # If not same control and target parameter we need iterations to reach
        # target
        elif target_parameter != control_parameter:
            
            if comp(target_value, target_end) or iterating:

                print "ITERATING TO FIND TARGET VALUE {}={}".format(\
                    target_parameter, target_end)
                # If first time add old values
                if not iterating:
                    interp_target_values.append(old_target_value)
                    interp_control_values.append(old_control_value)

                interp_target_values.append(target_value)
                interp_control_values.append(control_value)

                kind = {2:"linear", 3:"slinear", 4:"quadratic", 5:"cubic"}\
                       [len(interp_control_values)]

                # Sort and interpolate control value for target_end
                inds = np.argsort(interp_target_values)
                new_control = float(interp.interp1d(\
                    np.array(interp_target_values)[inds],
                    np.array(interp_control_values)[inds], kind)(target_end))

                control_step = new_control - control_value
                
                iterating = True
                save_me = False
                
        if save_me:
            collector.save()

    #collector.move_files()    

def passive(problem, collector=None, volume_control_params=None):
    # Do an initial solve
    case = problem.parameters["geometry"]["case"]
    problem.set_control_mode("pressure")
    solver = LVSolver()
    solver.solve(problem)
    if collector is None:
        collector = DataCollector("{}_passive".format(case), problem, "pressure", resdir)
        
    p_ED = read_target_pressure(case, datadir, "ED")
    
    pstep = p_ED / 5

    iter_target(problem, collector, p_ED, target_parameter="pressure",
                control_step=pstep, control_parameter="pressure",
                control_mode="pressure")

    collector.mark_stage("ED")
    
    #collector.dump_lists()
    #collector.plot()
    #collector.move_files()

    if volume_control_params:
        V_ED = read_target_volume(case, datadir, "ED")
        solver = LVSolver()
        solver.solve(problem)
        state_old = problem.state.copy(True)
        for control_param in volume_control_params:
            problem.state.assign(state_old)
            #collector = DataCollector("{}_passive_volume_control_{}".format(\
            #    case, control_param), problem, control_param)
            step = -problem.get_control_parameter(control_param)/5.
            iter_target(problem, collector, V_ED, target_parameter="volume",
                        control_step=step, control_parameter=control_param,
                        control_mode="pressure")
            #collector.dump_lists()
            #collector.plot()

def active(problem, collector=None):
    case = problem.parameters["geometry"]["case"]
    if collector is None:
        collector = DataCollector("{}_active".format(case), problem, "gamma", resdir)
        
    p_ES = read_target_pressure(case, datadir, "ES")
    V_ES = read_target_volume(case, datadir, "ES")

    step = 0.02
    iter_target(problem, collector, p_ES, target_parameter="pressure",
                control_step=step, control_parameter="gamma",
                control_mode="volume")
    
    collector.mark_stage("EIV")
    iter_target(problem, collector, V_ES, target_parameter="volume",
                control_step=step, control_parameter="gamma",
                control_mode="pressure")
    
    collector.mark_stage("ES")
    
    collector.dump_lists()
    collector.plot([[read_target_volume(case, datadir, "DS"), \
                     read_target_volume(case, datadir, "ED"), V_ES],
                    [read_target_pressure(case, datadir, "DS"),
                     read_target_pressure(case, datadir, "ED"), p_ES]])

if __name__ == "__main__" :

    meshdir = "./meshes"
    datadir = "./data"
    resdir  = "./results"

    set_log_level(PROGRESS)
    #cases = ["1017", "1024", "0912", "0917"]
    #cases = ["0917", "1017"]
    #cases = ["0912"]
    cases = ["1017"]
    #cases = [ "1024" ]
    #cases = ["1017", "0912", "0917"]

    # initialize the problem
    params = LVProblem.default_parameters()
    params["geometry"]["meshdir"] = meshdir

    # Default material parameters
    orig_mat_params = { k : float(params['material'][k])
                        for k in [ 'a', 'b', 'a_f', 'b_f', 'gamma' ] }
    
    # Set material parameters
    mat_params = dict((case, orig_mat_params.copy()) for case in cases)
    if "0912" in cases:
        mat_params["0912"]["a"] /= 2
        mat_params["0912"]["b"] /= 2

    # Use already fitted values. If these are not given a_f will be used to control
    # iteration to correct volume during passive inflation.
    control_for_volume = True
    volume_control_parameters = ["a_f"]
    
    if not control_for_volume:
        volume_control_parameters = None
        if "0912" in cases:
            mat_params["0912"]["a_f"] = 4.332860729463933
        if "0917" in cases:
            mat_params["0917"]["a_f"] = 2.8523524776107707
        if "1017" in cases:
            mat_params["1017"]["a_f"] = 3.385144444218423
        if "1024" in cases:
            mat_params["1024"]["a_f"] = 14.7746858397

    for case in cases:
        print 
        print "RUNNING CASE", case
        # Update params
        params["geometry"]["case"] = case

        # Instantiate the proble
        problem = LVProblem(**params)
        problem.set_control_parameters(**mat_params[case])

        # Collect data
        collector = DataCollector("{}_full_cycle".format(case),
                                  problem, ["gamma", "b_f", "a_f", "a", "b"],
                                  resdir)

        # Passive inflation
        passive(problem, collector, volume_control_parameters)
        #exit(0)

        # Active iso volumetric and isotonic contraction
        active(problem, collector)

        collector.move_files()    

    plt.show()
