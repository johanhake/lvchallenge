from fenics import *
import numpy as np

# vtk is required only for post-processing
try :
    from vtk import *
    from tvtk.array_handler import *
    from mesh_utils import *
    has_vtk = True
except ImportError :
    has_vtk = False

class ProlateGeometry() :
    def __init__(self, **kwargs) :
        # setup parameters
        p = self.default_parameters()
        p.update(**kwargs)
        self.parameters = p

        # load the mesh (by default in cm)
        h5name = "{meshdir}/{case}.h5".format(**p)
        h5file = HDF5File(mpi_comm_world(), h5name, "r")

        mesh_name = "{meshdir}/{case}{stage}.xml.gz".format(**p)
        self.mesh = Mesh()
        h5file.read(self.mesh, "{stage}/mesh".format(**p), True)

        self.domain = self.mesh.ufl_domain()

        # boundary markers
        self.BASE = h5file.attributes('{stage}/facets markers'.format(**p))['base']
        self.ENDO = h5file.attributes('{stage}/facets markers'.format(**p))['endo']
        self.EPI  = h5file.attributes('{stage}/facets markers'.format(**p))['epi']

        self.ENDORING = h5file.attributes('{stage}/ridges markers'.format(**p))['endoring']
        self.EPIRING  = h5file.attributes('{stage}/ridges markers'.format(**p))['epiring']

        self.bfun = MeshFunction("size_t", self.mesh, 2)
        h5file.read(self.bfun, '{stage}/facets markers'.format(**p))

        self.rfun = MeshFunction("size_t", self.mesh, 1)
        h5file.read(self.rfun, '{stage}/ridges markers'.format(**p))

        # fibers
        if p['fibers_at_nodes'] :
            fgroup = "{stage}/fibers at nodes".format(**p)
            if h5file.has_dataset(fgroup) :
                V = VectorFunctionSpace(self.mesh, "P", 1)
                self.f0 = Function(V)
                h5file.read(self.f0, fgroup)
            else :
                self.f0 = None
        else :
            fgroup = "{stage}/fibers at quadratures".format(**p)
            if h5file.has_dataset(fgroup) :
                degree = h5file.attributes(fgroup)['degree']
                V = VectorFunctionSpace(self.mesh, "Quadrature", degree)
                self.f0 = Function(V)
                h5file.read(self.f0, fgroup)
            else :
                self.f0 = None

    @staticmethod
    def default_parameters() :
        p = { 'meshdir' : './meshes',
              'case'    : '1024',
              'stage'   : 'DS',
              'long_axis' : 'x',
              'fibers_at_nodes' : False }

        return p

    def inner_volume(self, u = Constant((0.0, 0.0, 0.0))) :
        """
        Compute the inner volume of the cavity for a given displacement u.
        """

        # current inner volume
        X = SpatialCoordinate(self.domain)
        N = FacetNormal(self.domain)

        x = X + u
        F = grad(x)
        n = cofac(F) * N

        ds_endo = ds(self.ENDO, subdomain_data = self.bfun)

        Vendo_form = -1/float(3) * inner(x, n) * ds_endo

        return assemble(Vendo_form)

    def wall_volume(self, u = Constant((0.0, 0.0, 0.0))) :
        """
        Compute the volume of the wall.
        """

        X = SpatialCoordinate(self.domain)
        x = X + u
        F = grad(x)
        J = det(F)

        V_form = J * dx(domain = self.domain)

        return assemble(V_form)

    def ventricle_axial_length(self, surf, u = None) :
        """
        Compute the length of the ventricle by intersecting it with the
        line directed as its axis.
        """

        if not has_vtk :
            raise NotImplemented

        # restrict on boundary
        marker = { "endo" : self.ENDO, "epi" : self.EPI }[surf]
        meshb = boundary_mesh(self.mesh, self.bfun, marker)
        if u :
            ub = restrict_on_boundary(meshb, u)
        else :
            ub = None

        # vtk grid
        grid = dolfin2vtk(meshb, ub)
        lingeo = linear_grid_filter(grid, 1)

        long_axis = [ 'x', 'y', 'z' ].index(self.parameters['long_axis'])

        return lingeo.GetOutput().GetBounds()[2*long_axis + 1]
    def average_radius(self, surf, cut_quote, u = None) :
        """
        Compute average radius of an axial section of the ventricle.
        """

        if not has_vtk :
            raise NotImplemented

        # restrict on boundary
        marker = { "endo" : self.ENDO, "epi" : self.EPI }[surf]
        meshb = boundary_mesh(self.mesh, self.bfun, marker)
        if u :
            ub = restrict_on_boundary(meshb, u)
        else :
            ub = None

        # vtk grid
        grid = dolfin2vtk(meshb, ub)
        lingeo = linear_grid_filter(grid, 1)

        # cut plane
        long_axis = [ 'x', 'y', 'z' ].index(self.parameters['long_axis'])
        origin = [ 0.0, 0.0, 0.0 ]
        normal = [ 0.0, 0.0, 0.0 ]
        origin[long_axis] = cut_quote
        normal[long_axis] = 1.0
        plane = vtkPlane()
        plane.SetOrigin(*origin)
        plane.SetNormal(*origin)

        plane_cut = vtkCutter()
        plane_cut.SetInputData(lingeo.GetOutput())
        plane_cut.SetCutFunction(plane)
        plane_cut.Update()

        rsum = 0.0
        pts = plane_cut.GetOutput().GetPoints()
        for i in xrange(0, pts.GetNumberOfPoints()) :
            rsum += np.linalg.norm(pts.GetPoint(i)[1:])

        return rsum / pts.GetNumberOfPoints()

