from fenics import *

class LVSolver(object) :
    def __init__(self, use_snes=True, max_iterations=14) :
        self.reset(use_snes, max_iterations=max_iterations)

    def solve(self, problem) :
        return self.solver.solve(problem, problem.state.vector())

    def reset(self, use_snes=True, max_iterations=14) :
        if use_snes :
            solver = PETScSNESSolver()
            solver.parameters["report"] = False
            PETScOptions.set("snes_monitor")
        else :
            solver = NewtonSolver()
        solver.parameters["linear_solver"] = "umfpack"
        solver.parameters["maximum_iterations"] = max_iterations
        solver.parameters["lu_solver"]["same_nonzero_pattern"] = True
        solver.parameters["lu_solver"]["symmetric"] = False
        self.solver = solver


