from prolate_geometry import ProlateGeometry
from triso_material import ActiveTrIsotropicMaterial
from edge_type_bc import *
from realspaces import *
from fenics import *

class LVProblem(NonlinearProblem) :
    def __init__(self, **kwargs) :
        super(LVProblem, self).__init__()

        params = self.default_parameters()
        params.update(**kwargs)
        self.parameters = params

        # geometry
        ffc_par = self.parameters['form_compiler_parameters']
        geo_par = self.parameters['geometry']
        geo_par['fibers_quadrature_degree'] = ffc_par['quadrature_degree']
        self.geo = ProlateGeometry(**geo_par)

        # material (setting fibers)
        mat_par = self.parameters['material']
        mat_par['f0'] = self.geo.f0
        self.mat = ActiveTrIsotropicMaterial(**mat_par)

        self._init_spaces()
        self.state = Function(self._Mspace)
        self._init_forms()

    @staticmethod
    def default_parameters() :
        p = { 'form_compiler_parameters' : { 
              'quadrature_degree' : 4,
              'cpp_optimize' : True,
              'precompute_basis_const' : True,
              'optimize' : True,
              'cpp_args' : "-O3 --fast-math",
            },
              'output_dir' : 'results/passive',
              'bc_type' : 'fix_base_ver',
              'control_volume' : False,
              'geometry' : ProlateGeometry.default_parameters(),
              'material' : ActiveTrIsotropicMaterial.default_parameters()}
        return p

    def set_control_mode(self, control_mode):
        assert control_mode in ["pressure", "volume"]
        self._change_mode_and_reinit(control_volume = control_mode == "volume")

    def F(self, b, x) :
        ffc_params = self.parameters['form_compiler_parameters']
        assemble(self._G, tensor = b, \
                 form_compiler_parameters = ffc_params)
        if self._bcs : self._bcs.apply(b)

    def J(self, A, x) :
        ffc_params = self.parameters['form_compiler_parameters']
        assemble(self._dG, tensor = A, \
                 form_compiler_parameters = ffc_params)
        if self._bcs : self._bcs.apply(A)

    def set_pendo(self, p) :
        param = self.parameters
        if param['control_volume']:
            pnum = self._Mspace.num_sub_spaces() - 1
            set_realspace_var(self.state, pnum, p)
        else :
            param['p_endo'].assign(p)

    def set_Vendo(self, V) :
        param = self.parameters
        if not param['control_volume'] :
            raise RuntimeError('Cannot assign Vendo!')
        else :
            param['V_endo'].assign(V)

    def get_pendo(self) :
        param = self.parameters
        if param['control_volume'] :
            pnum = self._Mspace.num_sub_spaces() - 1
            return get_realspace_var(self.state, pnum)
        else :
            return float(param['p_endo'])

    def get_Vendo(self) :
        param = self.parameters
        if not param['control_volume'] :
            geo = self.geo
            u = split(self.state)[0]
            return geo.inner_volume(u)
        else :
            return float(param['V_endo'])

    def set_control_parameters(self, **kwargs):

        for param, value in kwargs.items():

            if param in ["pressure", "volume"]:
                if param == "pressure":
                    if self.parameters['control_volume']:
                        error("Problem is in volume control mode. Cannot use "\
                              "pressure as control.")
                    self.set_pendo(value)
                else:
                    if not self.parameters['control_volume']:
                        error("Problem is in pressure control mode. Cannot use "\
                              "volume as control.")
                    self.set_Vendo(value)
            else:
                if param not in self.mat.parameters:
                    error("{} is not a parameters.".format(param))

                self.mat.parameters[param].assign(value)

    def get_control_parameter(self, param):
        if param in ["pressure", "volume"]:
            if param == "pressure":
                return self.get_pendo()
            
            else:
                return self.get_Vendo()

        if param not in self.mat.parameters:
            error("{} is not a control parameter.".format(param))

        return float(self.mat.parameters[param])

    def get_displacement(self) :
        return self.state.split()[0]

    def _init_forms(self) :
        geo = self.geo
        mat = self.mat
        mesh = geo.mesh
        param = self.parameters

        # unknowns
        z = self.state
        u = split(z)[0]
        p = split(z)[1] if self.mat.incompressible() else None
        
        if param['control_volume'] :
            pendo = split(z)[-1]
            param['V_endo'] = Constant(geo.inner_volume())
            Vendo = param['V_endo']
            
        else :
            param['p_endo'] = Constant(0.0)
            pendo = param['p_endo']
            Vendo = None

        # internal energy
        # ---------------
        Lmat = mat.strain_energy(u, p) * dx

        # inner pressure
        # --------------
        Lvol = self._inner_volume_constraint(u, pendo, Vendo, geo.ENDO)

        # bcs
        # ---
        if param['bc_type'] == 'fix_endoring' :
            # no rigid motions
            Lrig = 0
            
            # fix the endocardium ring in all directions
            endoring = pick_endoring_bc()(geo.rfun, geo.ENDORING)
            Vsp = self._Mspace.sub(0)
            val = Constant((0.0, 0.0, 0.0))
            bcs = DirichletBC(Vsp, val, endoring, method = "pointwise")
            
        elif param['bc_type'] == 'fix_base_ver' :
            c = split(z)[2]
            
            # no traslations in yz-plane
            ct = as_vector([ 0.0, c[0], c[1] ])

            # no rotations around x-axis
            cr = as_vector([ c[2], 0.0, 0.0 ])
            Lrig = self._rigid_motion_constraints(u, ct, cr)

            # no vertical displacement at the base
            Vsp = self._Mspace.sub(0).sub(0)
            bcs = DirichletBC(Vsp, Constant(0.0), geo.bfun, geo.BASE)
            
        elif param['bc_type'] == 'free' :

            # 6 rigid motions to be removed
            raise NotImplemented
        
        else:
            raise RuntimeError('Invalid bc type!')

        # tangent problem
        # ---------------
        L = Lmat + Lvol + Lrig

        self._G   = derivative(L, z)
        self._dG  = derivative(self._G, z)
        self._bcs = bcs

    def _init_spaces(self) :
        mesh = self.geo.mesh
        param = self.parameters

        vlist = []
        vlist += [ VectorFunctionSpace(mesh, "CG", 2) ]

        if self.mat.incompressible() :
            vlist += [ FunctionSpace(mesh, "CG", 1) ]

        vlist += { 
            'fix_endoring' : [],
            'fix_base_ver' : [ VectorFunctionSpace(mesh, "Real", 0, 3) ],
            'free'         : [ VectorFunctionSpace(mesh, "Real", 0, 6) ],
            }[param['bc_type']]

        if param['control_volume'] :
            vlist += [ FunctionSpace(mesh, "Real", 0) ]

        self._Mspace = MixedFunctionSpace(vlist)

    @staticmethod
    def _rigid_motion_constraints(u, ct, cr) :
        """
        Compute the form
            (u, ct) * dx + (cross(u, X), cr) * dx
        where
            u  = displacement
            ct = Lagrange multiplier for translations
            ct = Lagrange multiplier for rotations
        """

        dom = u.domain()
        dim = dom.geometric_dimension()
        X = SpatialCoordinate(dom)

        Lt = inner(ct, u) * dx

        if dim == 2 :
            # rotations around z
            Lr = inner(cr, X[0]*u[1] - X[1]*u[0]) * dx
        elif dim == 3:
            # rotations around x, y, z
            Lr = inner(cr, cross(X, u)) * dx

        return Lt + Lr

    @staticmethod
    def _inner_volume_constraint(u, p, V, sigma) :
        """
        Compute the form
            (V(u) - V, p) * ds(sigma)
        where V(u) is the volume computed from u and
            u = displacement
            V = volume enclosed by sigma
            p = Lagrange multiplier
        sigma is the boundary of the volume.
        """

        dom = u.domain()
        dim = dom.geometric_dimension()
        X = SpatialCoordinate(dom)
        N = FacetNormal(dom)

        mesh = dom.data()
        bfun = MeshFunction("size_t", mesh, dim-1, mesh.domains())

        # ufl doesn't support any measure for duality
        # between two Real spaces, so we have to divide
        # by the total measure of the domain
        ds_sigma = ds(sigma, domain = dom, subdomain_data = bfun)
        area = assemble(Constant(1.0) * ds_sigma)

        # in detail, we impose the constraint
        #   ( 1/dim (x, n) + 1/area V ) * p = 0
        x = X + u
        I = Identity(dim)
        F = I + grad(u)
        n = cofac(F)*N

        V_u = Constant(1.0/dim) * inner(x, n)

        if V is None :
            L = p * V_u * ds_sigma
        else :
            L = p * (Constant(1.0/area) * V + V_u) * ds_sigma
        
        return L


    def _change_mode_and_reinit(self, control_volume = False):
        if self.parameters['control_volume'] == control_volume:
            return
        # save the current state
        state_old = self.state.copy(True)
        pendo_old = self.get_pendo()
        Vendo_old = self.get_Vendo()

        # reinit problem
        self.parameters['control_volume'] = control_volume
        self._init_spaces()
        self.state = Function(self._Mspace)
        self._init_forms()

        # assign old values
        assign(self.state.sub(0), state_old.sub(0))
        assign(self.state.sub(1), state_old.sub(1))
        if self.parameters['bc_type'] != 'fix_endoring' :
            assign(self.state.sub(2), state_old.sub(2))
        self.set_pendo(pendo_old)
        if control_volume :
            self.set_Vendo(Vendo_old)

