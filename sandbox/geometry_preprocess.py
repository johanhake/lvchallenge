from fenics import *
from os.path import isfile
import numpy as np

# boundary markers
BASE = 10
ENDO = 30
EPI  = 40
ENDORING = ENDO * BASE
EPIRING  = EPI  * BASE

def mark_endo_epi_rings(mesh, bfun) :
    endo_facets = np.where(bfun.array() == ENDO)[0]
    epi_facets  = np.where(bfun.array() == EPI)[0]
    base_facets = np.where(bfun.array() == BASE)[0]

    mesh.init(2, 1)
    topo = mesh.topology()(2, 1)

    endo_ridges = np.unique(reduce(np.union1d, map(topo, endo_facets)))
    epi_ridges  = np.unique(reduce(np.union1d, map(topo, epi_facets)))
    base_ridges = np.unique(reduce(np.union1d, map(topo, base_facets)))

    endoring_ridges = np.intersect1d(endo_ridges, base_ridges)
    epiring_ridges  = np.intersect1d(epi_ridges, base_ridges)

    rfun = MeshFunction("size_t", mesh, 1)
    rfun.set_all(0)
    rfun.array()[endoring_ridges] = ENDORING
    rfun.array()[epiring_ridges] = EPIRING

    return rfun

if __name__ == "__main__" :

    meshdir = '../meshes'

    for case in [ '1024', '1017', '0912', '0917' ] :
        h5name = "{}/{}.h5".format(meshdir, case)
        h5file = HDF5File(mpi_comm_world(), h5name, "w")

        for stage in [ 'DS', 'ED', 'ES' ] :

            # mesh
            mesh_name = "{}/{}{}.xml.gz".format(meshdir, case, stage)
            mesh = Mesh(mesh_name)

            # rescale to cm
            mesh.coordinates()[:] /= 10.0

            # fibers
            fn_name = "{}/{}{}_fibers_at_nodes.xml.gz".format(meshdir, case, stage)
            fq_name = "{}/{}{}_fibers_at_quadratures_4.xml.gz".format(meshdir, case, stage)

            if isfile(fn_name) :
                Vn = VectorFunctionSpace(mesh, "P", 1)
                fn = Function(Vn)
                File(fn_name) >> fn
            else :
                fn = None

            if isfile(fq_name) :
                Vq = VectorFunctionSpace(mesh, "Quadrature", 4)
                fq = Function(Vq)
                File(fq_name) >> fq
            else :
                fq = None

            # markers
            bfun = MeshFunction("size_t", mesh, 2, mesh.domains())
            bfun.array()[bfun.array() == max(bfun.array())] = 0
            mesh.domains().clear()

            rfun = mark_endo_epi_rings(mesh, bfun)

            # save everything
            h5file.write(mesh, "{}/mesh".format(stage))

            h5file.write(bfun, "{}/facets markers".format(stage))
            h5file.attributes("{}/facets markers".format(stage))["base"] = BASE
            h5file.attributes("{}/facets markers".format(stage))["endo"] = ENDO
            h5file.attributes("{}/facets markers".format(stage))["epi"] = EPI

            h5file.write(rfun, "{}/ridges markers".format(stage))
            h5file.attributes("{}/ridges markers".format(stage))["endoring"] = ENDORING
            h5file.attributes("{}/ridges markers".format(stage))["epiring"] = EPIRING

            if fn :
                h5file.write(fn, "{}/fibers at nodes".format(stage))
            if fq :
                h5file.write(fq, "{}/fibers at quadratures".format(stage))
                h5file.attributes("{}/fibers at quadratures".format(stage))["degree"] = 4

