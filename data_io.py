from lv_problem import LVProblem
from fenics import *
import numpy as np
import cPickle

stages = ["DS", "ED", "ES"]

def load_field(datadir, meshdir, outdir,
               case, field, stages = [ "ED", "ES" ]) :
    
    # initialize the problem
    params = LVChallengeProblem.default_parameters()
    params["geometry"]["case"] = case
    params["geometry"]["meshdir"] = meshdir
    params["geometry"]["datadir"] = datadir
    problem = LVChallengeProblem(**params)
    problem.set_control_mode("pressure")
    
    h5name = "{}/{}_full_cycle_u_save.h5".format(outdir, case)
    cpname = "{}/{}_full_cycle_pv.cpickle".format(outdir, case)

    h5file = HDF5File(mpi_comm_world(), h5name, "r")
    h5file.read(problem.state, "/initial")

    with open(cpname, "r") as f :
        markers = cPickle.load(f)["stage_markers"]

    ret = []
    for stage in stages:
        tag = "values_{}".format(markers[stage])
        h5file.read(problem.state.vector(), tag, True)
        ret.append(problem.state.sub(field, deepcopy = True))

    return ret

def load_displacements(datadir, meshdir, outdir,
                       case, stages = [ "ED", "ES" ]) :

    return load_field(datadir, meshdir, outdir, case, 0, stages)

def load_pressures(datadir, meshdir, outdir,
                  case, stages = [ "ED", "ES" ]) :
    
    return load_field(datadir, meshdir, outdir, case, 1, stages)

def load_material_parameters(outdir, case, stage) :
    cpname = "{}/{}_full_cycle_pv.cpickle".format(outdir, case)
    with open(cpname, "r") as f :
        p = cPickle.load(f)
    idx = p['stage_markers'][stage]
    return { k : p[k][idx] for k in [ 'a', 'b', 'a_f', 'b_f', 'gamma' ] }

def load_geometry_transformation(meshdir, case) :
    
    fname = "{}/{}_transform.txt".format(meshdir, case)
    T = np.genfromtxt(fname, delimiter = ',', skip_footer = 1)
    x_shift = np.genfromtxt(fname, skip_header = 3, usecols = (1, ),
                            delimiter = "=")
    b = np.array([x_shift, 0.0, 0.0])

    return T, b

def read_target_bc_dirichlet(case, datadir, stage = "ED") :

    fname = "{}/{}_InputData/"\
            "LV_BC/LV_Displacement_BC_{}.txt".format(datadir, case, stage)
    
    bcs = np.genfromtxt(fname, skiprows = 1,
                        dtype = [('id', 'int'), ('u', 'float', 3)])
    
    return bcs

def read_target_pressure(case, datadir, stage="ED"):
    assert stage in stages
    ind = stages.index(stage)
    p = np.genfromtxt("{}/{}_InputData/"\
                         "LV_BC/LV_Pressure.txt".format(datadir, case), usecols=(1,))[ind]
    return p

def read_target_volume(case, datadir, stage="ED"):
    assert stage in stages
    #geom = ProlateGeometry(case=case, meshdir=meshdir, stage=stage)
    #return geom.inner_volume()
    
    ind = stages.index(stage)
    return np.genfromtxt("{}/{}_InputData/"\
                         "LV_BC/LV_Volume.txt".format(datadir, case), usecols=(1,))[ind]

def read_hexa_mesh_connectivity(case, datadir) :

    fname = "{}/{}_InputData/"\
            "LV_Fibre/ProcessedDTIData/LV_Hex_Mesh_Connectivity.txt"\
            .format(datadir, case)

    # read the file
    conn = np.loadtxt(fname, skiprows = 1, dtype = np.uintp)[:, 1:]
    # connectivity starts from 1, shift
    conn -= 1

    return conn

def read_hexa_mesh_xyz(case, datadir) :

    fname = "{datadir}/{case}_InputData/"\
            "LV_Fibre/ProcessedDTIData/DS_{case}_FibreVector.txt"\
            .format(datadir = datadir, case = case)

    # read the file
    return np.loadtxt(fname, skiprows = 1)[:, 1:4]

def read_hexa_mesh_fibers(case, datadir) :

    fname = "{datadir}/{case}_InputData/"\
            "LV_Fibre/ProcessedDTIData/DS_{case}_FibreVector.txt"\
            .format(datadir = datadir, case = case)

    # read the file
    return np.loadtxt(fname, skiprows = 1)[:, 4:]
def read_xyz_surface(case, stage, surf, datadir) :
    # reading data
    fname = '{}/{}_InputData/LV_Geometry/Geometry_Surface/{}_{}.txt'\
            .format(datadir, case, stage, surf)

    return np.loadtxt(fname, skiprows = 1)

