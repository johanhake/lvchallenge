from fenics import *
import math as m

class ActiveTrIsotropicMaterial(object) :

    def __init__(self, **kwargs) :
        p = self.default_parameters()
        p.update(kwargs)

        self.parameters = p

    @staticmethod
    def default_parameters() :
        p = { 'a' : Constant(0.2362),
              'b' : Constant(10.810),
              'a_f' : Constant(20.037),
              'b_f' : Constant(0.876),
              'kappa' : float('Inf'),
              'gamma' : Constant(0.0),
              'f0' : Constant((1.0, 0.0, 0.0)) } 
        return p

    def strain_energy(self, u, p = None) :

        param = self.parameters
        if self.incompressible() and p is None :
            raise RuntimeError("Incompressible material needs pressure!")

        F = Identity(3) + grad(u)
        J = det(F)
        C = F.T * F

        # volumetric-isochoric decomposition
        Jm23 = pow(J, -float(2)/3)

        # invariants
        I_1  = Jm23 * tr(C)
        I_4f = Jm23 * inner(C*param['f0'], param['f0'])

        # activated invariants
        mgamma = 1.0 - param['gamma']
        Ie_1  = mgamma * I_1 + (1/pow(mgamma, 2) - mgamma) * I_4f
        Ie_4f = 1/pow(mgamma, 2) * I_4f

        # strain energy
        a = param['a']
        b = param['b']
        a_f = param['a_f']
        b_f = param['b_f']

        subplus = lambda u : conditional(ge(u, 0.0), u, 0)

        W_1  = a/(2.0*b) * (exp(b*(Ie_1 - 3))-1)
        W_4f = a_f/(2.0*b_f) * (exp(b_f*pow(subplus(Ie_4f - 1), 2)) - 1)

        Wint = W_1 + W_4f
        if self.incompressible() :
            Winc = - p * (J - 1)
        else :
            kappa = Constant(param['kappa'])
            Winc = kappa/4.0 * (J**2 - 1 - 2.0*ln(J))

        return Wint + Winc

    def incompressible(self) :
        return m.isinf(self.parameters['kappa'])

