from fenics import *
import shutil

class DataCollector:
    def __init__(self, casename, problem,
                       control_parameters = None,
                       resdir = "./results"):

        self.problem = problem
        self.basename = "{}/{}".format(resdir, casename)
        self.ufile = File("{}_u_plot.xdmf".format(self.basename))
        self.h5_save = HDF5File(mpi_comm_world(), \
                                "{}_u_save_running.h5".format(self.basename), "w")

        self.stage_markers = dict(DS=0)
        
        if control_parameters is None:
            self.control_parameters = {}
        else:
            self.control_parameters = dict((param, []) \
                                           for param in control_parameters \
                                           if param not in ["pressure", "volume"])
        
        # Always store pressure and volume
        self.Vlist = [problem.get_Vendo()]
        self.plist = [problem.get_pendo()]
        
        for param in self.control_parameters.keys():
            self.control_parameters[param].append(problem.get_control_parameter(param))

        self.counter = 0
        self.h5_save.write(problem.state, "/initial")
        u_out = self.problem.get_displacement()
        self.ufile << (u_out, 1.*self.counter)

    def output_params(self):
        pendo = self.problem.get_pendo()
        Vendo = self.problem.get_Vendo()
        pv_str = 'p = {}, V = {}'.format(pendo, Vendo)
        if self.control_parameters:
            pv_str = ", ".join([pv_str, ", ".join("{} = {}".format(\
                param, self.problem.get_control_parameter(param)) \
                            for param in self.control_parameters)])
        info(pv_str)

    def mark_stage(self, stage):
        self.stage_markers[stage] = self.counter

    def save(self):
        self.counter += 1
        self.h5_save.write(self.problem.state.vector(), "/values_{}".format(\
            self.counter))
            
        self.Vlist.append(self.problem.get_Vendo())
        self.plist.append(self.problem.get_pendo())
        for param in self.control_parameters.keys():
            self.control_parameters[param].append(self.problem.get_control_parameter(param))
            
        u_out = self.problem.get_displacement()
        self.ufile << (u_out, 1.*self.counter)

    def dump_lists(self):
        if MPI.rank(mpi_comm_world()) == 0:

            import cPickle
            data = dict(Vlist=self.Vlist, plist=self.plist)
            for param, values in self.control_parameters.items():
                data[param] = values
            data["stage_markers"] = self.stage_markers
            cPickle.dump(data, open("{}_pv.cpickle".format(self.basename), "w"))
            
    def plot(self, extra_args=None):
        if MPI.rank(mpi_comm_world()) == 0:
            plt.figure()
            plt.plot(self.Vlist, self.plist, 'ro-')
            if extra_args is not None:
                extra_args.append('bo-')
                plt.plot(*extra_args)
            plt.xlabel("Vol [ml]")
            plt.ylabel(r"pressure [$\frac{N}{cm^2}$]")
            plt.savefig("{}_pv.pdf".format(self.basename))

    def move_files(self):
        try:
            shutil.move("{}_u_save_running.h5".format(self.basename),
                        "{}_u_save.h5".format(self.basename))
        except:
            pass
 
